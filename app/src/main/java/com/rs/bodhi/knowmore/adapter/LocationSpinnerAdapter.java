package com.rs.bodhi.knowmore.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.rs.bodhi.R;
import com.rs.bodhi.knowmore.model.Geoname;

import java.util.List;

public class LocationSpinnerAdapter extends ArrayAdapter<Geoname> {

    private List<Geoname> items;
    private Activity activity;

    public LocationSpinnerAdapter(Activity activity, List<Geoname> items) {
        super(activity, R.layout.spinner_row, items);
        this.items = items;
        this.activity = activity;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            v = inflater.inflate(R.layout.spinner_row, null);
        }
        TextView lbl = (TextView) v.findViewById(R.id.text1);
        lbl.setTextColor(Color.BLACK);
        lbl.setText(items.get(position).getPlaceName());
        return v;
    }

    @Override
    public Geoname getItem(int position) {
        return items.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            v = inflater.inflate(R.layout.spinner_row, null);
        }
        TextView lbl = (TextView) v.findViewById(R.id.text1);
        lbl.setTextColor(Color.BLACK);
        lbl.setText(items.get(position).getPlaceName());
        return v;
    }
}