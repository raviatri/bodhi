package com.rs.bodhi.knowmore;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.rs.bodhi.MainActivity;
import com.rs.bodhi.R;
import com.rs.bodhi.knowmore.adapter.LocationSpinnerAdapter;
import com.rs.bodhi.knowmore.model.Geoname;
import com.rs.bodhi.knowmore.model.LocationModel;
import com.rs.bodhi.tabbar.HomeActivity;
import com.rs.bodhi.utility.Constants;
import com.rs.bodhi.utility.Preferences;
import com.rs.bodhi.utility.Util;
import com.rs.bodhi.verifyotp.VerifyOtp;
import com.rs.bodhi.vollyclient.IResult;
import com.rs.bodhi.vollyclient.VolleyService;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class KnowMore extends AppCompatActivity implements View.OnClickListener {
    Button btnLetsStart,btnSkip;
    int year,month,day,DATE_PICKER_ID = 100, PROFILE_UPDATE_API = 1;
    TextView edtDob,txtTime, txtName;
    String userName, dobStr, placeOfBirth, birthTime, gender;
    Geoname selectedGeoName;
    LinearLayout dobContainer,timeContainer;
    Spinner genderSpinner;
    AutoCompleteTextView pobSpinner;
    private static final String[] paths = {"Please select your gender", "Male", "Female"};
    IResult mResultCallback = null;
    VolleyService mVolleyService;
    List<Geoname> locations = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_know_more);
        init();
    }

    private void init(){
        btnLetsStart = findViewById(R.id.btn_lets_start);
        btnSkip = findViewById(R.id.btn_skip);
        edtDob = findViewById(R.id.edit_dob);
        txtTime = findViewById(R.id.edit_time_of_birth);
        dobContainer = findViewById(R.id.dob_container);
        timeContainer = findViewById(R.id.time_container);
        genderSpinner = findViewById(R.id.spinner_gender);
        pobSpinner = findViewById(R.id.edit_place_of_birth);
        txtName = findViewById(R.id.edt_user_name);

        btnLetsStart.setOnClickListener(this);
        btnSkip.setOnClickListener(this);
        dobContainer.setOnClickListener(this);
        edtDob.setOnClickListener(this);
        timeContainer.setOnClickListener(this);
        txtTime.setOnClickListener(this);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(KnowMore.this,
                android.R.layout.simple_spinner_item,paths);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSpinner.setAdapter(adapter);

        pobSpinner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length()>2){
                    callApi(s.toString());
                }
            }
        });

        pobSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selection = locations.get(position).getPlaceName();
                selectedGeoName = locations.get(position);
                pobSpinner.setText(selection);
                Util.logData("selected item "+selection);
            }
        });
    }

    private void SelectDate() {
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        showDialog(DATE_PICKER_ID);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 100:
                // open datepicker dialog.
                // set date picker for current date
                // add pickerListener listner to date picker
                return new DatePickerDialog(this, pickerListener, year, month,day);
            default:
                throw new IllegalStateException("Unexpected value: " + id);
        }
    }

    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year  = selectedYear;
            month = selectedMonth;
            day   = selectedDay;

            // Show selected date
            edtDob.setText(new StringBuilder().append(month + 1)
                    .append("-").append(day).append("-").append(year)
                    .append(" "));

        }
    };

    private void showTime(){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(KnowMore.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                txtTime.setText( selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_skip:
                moveToHomeScene();

                //  Util.logData("spinner "+genderSpinner.getSelectedItem().toString());
                break;
            case R.id.btn_lets_start:
                // Validate all fields and call api.
                if (validateFields()){

                }
                break;
            case R.id.dob_container:
                SelectDate();
                break;
            case R.id.edit_dob:
                SelectDate();
                break;
            case R.id.edit_time_of_birth:
                showTime();
                break;
            case R.id.time_container:
                showTime();
                break;
        }
    }

    // Call profile update api.
    private void callProfileUpdateAPI(){
        if (Util.isNetworkConnected(this)) {
            try {
                JSONObject object = new JSONObject();
                object.put(Constants.NAME, userName);
                object.put(Constants.DOB, dobStr);
                object.put(Constants.GENDER, gender);
                object.put(Constants.TIME_OF_BIRTH, birthTime);
                object.put(Constants.PLACE_OF_BIRTH, placeOfBirth);
                object.put(Constants.LATTITUDE, selectedGeoName.getLatitude());
                object.put(Constants.LONGITUDE, selectedGeoName.getLongitude());
                object.put(Constants.TIME_ZONE_ID, selectedGeoName.getTimezoneId());
                object.put(Constants.COUNTRY_CODE, selectedGeoName.getCountryCode());
                mVolleyService = new VolleyService(mResultCallback, KnowMore.this);
                initVolleyCallback();
                Util.logData("API:- " + Constants.REGISTER);
                Util.logData("Request:- " + object.toString());
                mVolleyService.postDataVolley("1", Constants.REGISTER, object.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            Toast.makeText(this, "Please check your internet connectivity.", Toast.LENGTH_SHORT).show();
        }
    }

    // Validate all fields before calling update profile api.
    private boolean validateFields(){
        // Check for name fields.
        userName = txtName.getText().toString();
        if (TextUtils.isEmpty(userName)){
            Util.showAlertPopUpAndFinish(this, this.getString(R.string.validation_name));
            return  false;
        }
        // Check for date of birth.
        dobStr = edtDob.getText().toString();
        if (TextUtils.isEmpty(dobStr)){
            Util.showAlertPopUpAndFinish(this, this.getString(R.string.validation_dob));
            return  false;
        }
        // Check for place of birth.
        if (selectedGeoName != null){
            placeOfBirth = selectedGeoName.getPlaceName();
        }else{
            Util.showAlertPopUpAndFinish(this, this.getString(R.string.validation_place_of_birth));
            return  false;
        }
        // Check for birth time.
        birthTime = txtTime.getText().toString();
        if (TextUtils.isEmpty(birthTime)){
            Util.showAlertPopUpAndFinish(this, this.getString(R.string.validation_birth_time));
            return  false;
        }
        // Check for gender.
        gender = genderSpinner.getSelectedItem().toString();
        if (TextUtils.isEmpty(gender)){
            Util.showAlertPopUpAndFinish(this, this.getString(R.string.validation_gender));
            return  false;
        }
        return  true;
    }

    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0)
            return false;
        return true;
    }
    // Move to home scene.
    private void moveToHomeScene() {
        Intent intent1 = new Intent(KnowMore.this, HomeActivity.class);
        startActivity(intent1);
        finish();
    }

    private void callApi(String place){
        if (Util.isNetworkConnected(this)) {
            try {
                JSONObject object = new JSONObject();
                object.put("place", place);
                object.put("maxRows", Constants.MAX_ROW);
                mVolleyService = new VolleyService(mResultCallback, KnowMore.this);
                initVolleyCallback();
                Util.logData("API:- " + Constants.GET_LOCATION);
                Util.logData("Request:- " + object.toString());
                mVolleyService.postLocationDataVolley("10", Constants.GET_LOCATION, object.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            Toast.makeText(this, "Please check your internet connectivity.", Toast.LENGTH_SHORT).show();
        }
    }

    void initVolleyCallback() {
        mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String requestType, String response) {
                Util.logData("Response:- "+response);
                if (requestType.equals("1")){
                    parseProfileUpdateResponse(response);
                }else{
                    parseGeoLocationResponse(response);
                }
            }
            @Override
            public void notifyError(String requestType, VolleyError error) {
                //Toast.makeText(VerifyOtp.this, "Error occured", Toast.LENGTH_SHORT).show();
            }
        };
    }

    private void parseProfileUpdateResponse(String response){
        try{
            JSONObject object = new JSONObject(response);

            if (object.getInt("statusCode") == 200){
                JSONObject payLoad = object.getJSONObject(Constants.PAY_LOAD);
            }else{
                Toast.makeText(this, object.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void parseGeoLocationResponse(String response){
        GsonBuilder builder = new GsonBuilder();
        locations.clear();
        Type listType = new TypeToken<LocationModel>() {
        }.getType();
        LocationModel locationModel = builder.create().fromJson(response, listType);
        locations = locationModel.getGeonames();
        Util.logData(locationModel.getGeonames().get(0).getPlaceName());

        LocationSpinnerAdapter adapter = new LocationSpinnerAdapter(KnowMore.this,locations);

       // adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        pobSpinner.setAdapter(adapter);

        adapter.setNotifyOnChange(true);
        adapter.notifyDataSetChanged();
    }
}