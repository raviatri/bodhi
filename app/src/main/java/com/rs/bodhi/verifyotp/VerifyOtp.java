package com.rs.bodhi.verifyotp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.rs.bodhi.R;
import com.rs.bodhi.entermobilenumber.EnterMobileNumber;
import com.rs.bodhi.knowmore.KnowMore;
import com.rs.bodhi.splash.SplashActivity;
import com.rs.bodhi.tabbar.HomeActivity;
import com.rs.bodhi.utility.Constants;
import com.rs.bodhi.utility.Preferences;
import com.rs.bodhi.utility.Util;
import com.rs.bodhi.vollyclient.IResult;
import com.rs.bodhi.vollyclient.VolleyService;

import org.json.JSONObject;


// https://stackoverflow.com/questions/29860906/widget-appcompat-button-colorbuttonnormal-shows-gray/34517103#34517103
public class VerifyOtp extends AppCompatActivity {
    EditText otp1,otp2,otp3,otp4,otp5,otp6;
    Button btnSubmit;
    IResult mResultCallback = null;
    VolleyService mVolleyService;
    ProgressDialog dialog;
    String mobile,countryCode;
    String opt1Str ;
    String opt2Str ;
    String opt3Str ;
    String opt4Str ;
    String opt5Str ;
    String opt6Str ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);

        dialog = new ProgressDialog(this);
        dialog.setMessage("Please wait..");
        dialog.setCancelable(false);
        mobile = getIntent().getStringExtra("mobile");
        countryCode = getIntent().getStringExtra("countryCode");
        init();
    }

    private void init(){
        otp1 = findViewById(R.id.otp1);
        otp2 = findViewById(R.id.otp2);
        otp3 = findViewById(R.id.otp3);
        otp4 = findViewById(R.id.otp4);
        otp5 = findViewById(R.id.otp5);
        otp6 = findViewById(R.id.otp6);
        btnSubmit = findViewById(R.id.btn_submit);

        otp1.addTextChangedListener(new OtpTextWatcher(otp1));
        otp2.addTextChangedListener(new OtpTextWatcher(otp2));
        otp3.addTextChangedListener(new OtpTextWatcher(otp3));
        otp4.addTextChangedListener(new OtpTextWatcher(otp4));
        otp5.addTextChangedListener(new OtpTextWatcher(otp5));
        otp6.addTextChangedListener(new OtpTextWatcher(otp6));

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 opt1Str = otp1.getText().toString().trim();
                 opt2Str = otp2.getText().toString().trim();
                 opt3Str = otp3.getText().toString().trim();
                 opt4Str = otp4.getText().toString().trim();
                 opt5Str = otp5.getText().toString().trim();
                 opt6Str = otp6.getText().toString().trim();

                if (!opt1Str.isEmpty() && !opt2Str.isEmpty() && !opt3Str.isEmpty() && !opt4Str.isEmpty() && !opt5Str.isEmpty()
                        && !opt6Str.isEmpty()){
                    callApi();
                }else{
                    Toast.makeText(VerifyOtp.this, "Please enter you otp", Toast.LENGTH_SHORT).show();
                }


            }
        });
    }

    private void callApi(){
        if (Util.isNetworkConnected(this)) {
            try {
                JSONObject object = new JSONObject();
                object.put("countryCode", countryCode);
                object.put("phoneNumber", mobile);
                object.put("verificationCode", opt1Str+opt2Str+opt3Str+opt4Str+opt5Str+opt6Str);
                initVolleyCallback();
                mVolleyService = new VolleyService(mResultCallback, VerifyOtp.this);
                dialog = new ProgressDialog(VerifyOtp.this);
                dialog.setCancelable(false);
                dialog.setMessage("Please wait...");
                dialog.show();
                Util.logData("API:- " + Constants.VERIFY_OTP);
                Util.logData("Request:- " + object.toString());
                mVolleyService.postDataVolley("1", Constants.VERIFY_OTP, object.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            Toast.makeText(this, "Please check your internet connectivity.", Toast.LENGTH_SHORT).show();
        }
    }

    void initVolleyCallback() {
        mResultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, String response) {

                Util.logData("Response:- "+response);
                dialog.dismiss();
                parseResponse(response);
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                dialog.dismiss();
                Toast.makeText(VerifyOtp.this, "Error occured", Toast.LENGTH_SHORT).show();
            }
        };
    }

    private void parseResponse(String response){

        // {"statusCode":200,"error":false,"message":"","payLoad":{"isVerified":true,"isProfileUpdated":false,"isActive":true,"rating":-1,"cashBalance":0,"isLastPaymentFulfilled":true,"_id":"5faa2ced8a53be096a947d07","countryCode":"91","phoneNumber":"9015245665","__v":0,"accessToken":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoiNWZhYTJjZWQ4YTUzYmUwOTZhOTQ3ZDA3X185MjAxMzIiLCJpYXQiOjE2MDQ5ODk5NTgsImV4cCI6MTYwNzU4MTk1OH0.f4WRWys_pwcUkq9rr89GzEO295mCjSoZ_OamtwGLsTc"}}
        try{
            JSONObject object = new JSONObject(response);

            if (object.getInt("statusCode") == 200){
                JSONObject payLoad = object.getJSONObject(Constants.PAY_LOAD);

                // Fetch and save accessToken, isProfileUpdated, isVerified, MobileNumber and Country code.
                Preferences.saveAccessToken(VerifyOtp.this, payLoad.optString(Constants.ACCESS_TOKEN));
                boolean isProfileUpdated = payLoad.optBoolean(Constants.IS_PROFILE_UPDATED);
                Preferences.setIsProfileUpdated(VerifyOtp.this, isProfileUpdated);
                Preferences.setIsMobileNumberVerified(VerifyOtp.this, payLoad.optBoolean(Constants.IS_VERIFIED));
                Preferences.saveMobileNumber(VerifyOtp.this, payLoad.optString(Constants.MOBILE_NUMBER));
                Preferences.saveCountryCode(VerifyOtp.this, payLoad.optString(Constants.COUNTRY_CODE));

                if (isProfileUpdated){
                    showHomeScene();
                }else{
                    showRegistrationScene();
                }
            }else{
                Toast.makeText(this, object.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    private void showRegistrationScene() {
        Intent intent = new Intent(this, KnowMore.class);
        startActivity(intent);
        finish();
    }

    private void showHomeScene(){
        Intent i = new Intent(this, HomeActivity.class);
        startActivity(i);
        finish();
    }

    public class OtpTextWatcher implements TextWatcher {

        private View view;
        private OtpTextWatcher(View view)
        {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();
            switch(view.getId())
            {

                case R.id.otp1:
                    if(text.length()==1)
                        otp2.requestFocus();
                    break;
                case R.id.otp2:
                    if(text.length()==1)
                        otp3.requestFocus();
                    else if(text.length()==0)
                        otp1.requestFocus();
                    break;
                case R.id.otp3:
                    if(text.length()==1)
                        otp4.requestFocus();
                    else if(text.length()==0)
                        otp2.requestFocus();
                    break;
                case R.id.otp4:
                    if(text.length()==1)
                        otp5.requestFocus();
                    else if(text.length()==0)
                        otp3.requestFocus();
                    break;
                case R.id.otp5:
                    if(text.length()==1)
                        otp6.requestFocus();
                    else if(text.length()==0)
                        otp4.requestFocus();
                    break;
                case R.id.otp6:
                    if(text.length()==0)
                        otp5.requestFocus();
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }
    }
}