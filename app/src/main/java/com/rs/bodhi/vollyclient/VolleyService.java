package com.rs.bodhi.vollyclient;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import androidx.collection.ArrayMap;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class VolleyService {

    private IResult mResultCallback = null;
    private Context mContext;

    public VolleyService(IResult resultCallback, Context context) {
        mResultCallback = resultCallback;
        mContext = context;
    }

    public void postDataVolley(final String requestType, String url, final String requestBody) {
        try {

            if (checkNetworkConnectivity(mContext)) {

                RequestQueue queue = Volley.newRequestQueue(mContext);


                StringRequest strReq = new StringRequest(Request.Method.POST,
                        url, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        if (mResultCallback != null) {
                            mResultCallback.notifySuccess(requestType, response);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mResultCallback != null)
                            mResultCallback.notifyError(requestType, error);
//                    Utility.logDataVolleyError("VolleyError \n",error);
                    }
                }) {
                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8";
                    }

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody.toString().getBytes();
                        } catch (Exception uee) {
                            uee.printStackTrace();
                            return null;
                        }
                    }
                };

                strReq.setRetryPolicy(new DefaultRetryPolicy(
                        90000,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                queue.add(strReq);

            } else {
                Toast.makeText(mContext, "Please check internet connectivity", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getDataVolley(final String requestType, String url) {
        try {
            RequestQueue queue = Volley.newRequestQueue(mContext);

            StringRequest jsonObj = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (mResultCallback != null)
                        mResultCallback.notifySuccess(requestType, response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

            queue.add(jsonObj);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void postLocationDataVolley(final String requestType, String url, final String requestBody) {
        try {

            if (checkNetworkConnectivity(mContext)) {

                RequestQueue queue = Volley.newRequestQueue(mContext);


                StringRequest strReq = new StringRequest(Request.Method.POST,
                        url, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        if (mResultCallback != null) {
                            mResultCallback.notifySuccess(requestType, response);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (mResultCallback != null)
                            mResultCallback.notifyError(requestType, error);
//                    Utility.logDataVolleyError("VolleyError \n",error);
                    }
                }) {
//                    @Override
//                    public String getBodyContentType() {
//                        return "application/json; charset=utf-8";
//                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put("authorization", "Basic NjE0NzQ0OjUyNGMwYmNiZjM1NWRlNDk5OGE5YTFjNzYzNDFhZGUw");
                        params.put("Content-Type", "application/json");
                        return params;
                    }

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody.toString().getBytes();
                        } catch (Exception uee) {
                            uee.printStackTrace();
                            return null;
                        }
                    }
                };

                strReq.setRetryPolicy(new DefaultRetryPolicy(
                        90000,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                queue.add(strReq);

            } else {
                Toast.makeText(mContext, "Please check internet connectivity", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean checkNetworkConnectivity(Context context) {
        if (context == null)
            return false;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
