package com.rs.bodhi.entermobilenumber;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.hbb20.CountryCodePicker;
import com.rs.bodhi.R;
import com.rs.bodhi.tabbar.HomeActivity;
import com.rs.bodhi.utility.Constants;
import com.rs.bodhi.utility.Util;
import com.rs.bodhi.verifyotp.VerifyOtp;
import com.rs.bodhi.vollyclient.IResult;
import com.rs.bodhi.vollyclient.VolleyService;

import org.json.JSONObject;

public class EnterMobileNumber extends AppCompatActivity {
    IResult mResultCallback = null;
    VolleyService mVolleyService;
    ProgressDialog dialog;
    EditText edtMobileNumber;
    Button btnSendOtp,btnSkip;
    CountryCodePicker countryCodePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_mobile_number);
        dialog = new ProgressDialog(this);
        dialog.setMessage("Please wait..");
        dialog.setCancelable(false);
        init();
    }

    private void init(){
        edtMobileNumber = findViewById(R.id.edt_mobile);
        btnSendOtp = findViewById(R.id.btn_send_otp);
        countryCodePicker = findViewById(R.id.ccp);

        btnSendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edtMobileNumber.getText().toString().trim().length()==10){

                    callApi();
                }else{
                    Toast.makeText(EnterMobileNumber.this, "Please enter a valid mobile number", Toast.LENGTH_SHORT).show();
                }

            }
        });

        findViewById(R.id.btn_skip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EnterMobileNumber.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void callApi(){
        if (Util.isNetworkConnected(this)) {
            try {
                JSONObject object = new JSONObject();
                object.put("countryCode", countryCodePicker.getSelectedCountryCode());
                object.put("phoneNumber", edtMobileNumber.getText().toString().trim());
                initVolleyCallback();
                mVolleyService = new VolleyService(mResultCallback, EnterMobileNumber.this);
                dialog = new ProgressDialog(EnterMobileNumber.this);
                dialog.setCancelable(false);
                dialog.setMessage("Please wait...");
                dialog.show();
                Util.logData("API:- " + Constants.LOGIN);
                Util.logData("Request:- " + object.toString());
                mVolleyService.postDataVolley("1", Constants.LOGIN, object.toString());


            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            Toast.makeText(this, "Please check your internet connectivity.", Toast.LENGTH_SHORT).show();
        }
    }

    void initVolleyCallback() {
        mResultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, String response) {

                Util.logData("Response:- "+response);
                dialog.dismiss();
                parseResponse(response);
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                dialog.dismiss();
                Toast.makeText(EnterMobileNumber.this, "Error occured", Toast.LENGTH_SHORT).show();
            }
        };
    }

    private void parseResponse(String response){

        // {"statusCode":200,"error":false,"message":"Otp sent Successfully"}
        try{
            JSONObject object = new JSONObject(response);

            if (object.getInt("statusCode") == 200){
                Toast.makeText(this, object.getString("message"), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, VerifyOtp.class);
                intent.putExtra("mobile",edtMobileNumber.getText().toString().trim());
                intent.putExtra("countryCode",countryCodePicker.getSelectedCountryCode());
                startActivity(intent);
                finish();
            }else{
                Toast.makeText(this, object.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }catch(Exception e){
            e.printStackTrace();
        }

    }

}