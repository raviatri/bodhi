package com.rs.bodhi.utility;

public class Constants {
    public static final String BASE_URL = "http://13.127.73.226:3000/v1/";
    public static final String LOGIN = BASE_URL+"users/login";
    public static final String VERIFY_OTP = BASE_URL+"users/verify-login-otp";
    public static final String GET_LOCATION = "https://json.astrologyapi.com/v1/geo_details";
    public static final String FORCE_UPDATE = BASE_URL+"centralValues/check-force-update";
    public static final String REGISTER = BASE_URL+"users";

    public static final int MAX_ROW = 4;

    public static final String APP_NAME = "Bodhi";
    public static final String BLANK_STRING = "";
    public static final String PAY_LOAD = "payLoad";
    public static final String ALREADY_UPDATED_WS = "alreadyUpdated";
    public static final String IS_FORCE_UPDATE = "isForceUpdate";
    public static final String FORCE_UPDATE_MESSAGE = "forceUpdateMessage";
    public static final String IS_VERIFIED = "isVerified";
    public static final String IS_PROFILE_UPDATED = "isProfileUpdated";
    public static final String ACCESS_TOKEN = "accessToken";
    public static final String MOBILE_NUMBER = "phoneNumber";
    public static final String COUNTRY_CODE = "countryCode";
    public static final String NAME = "name";
    public static final String DOB = "dob";
    public static final String GENDER = "gender";
    public static final String TIME_OF_BIRTH = "timeOfBirth";
    public static final String PLACE_OF_BIRTH = "placeOfBirth";
    public static final String LATTITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String TIME_ZONE_ID = "timeZoneId";

}
