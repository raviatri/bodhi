package com.rs.bodhi.utility;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.util.Log;

import com.rs.bodhi.R;

public class Util {

    public static void logData(String log){
        Log.e("logdata",log);
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    /* Method for getting the application version name */
    public static String getAppVersion(Context context) {
        String appVersion = "";
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            appVersion = info.versionName;
        } catch (Exception e) {
            //e.printStackTrace();
        }
        return appVersion;
    }

    /* Method for getting the application Version Code */
    public static int getAppVersionCode(Context context) {
        int appVersion = 0;
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            appVersion = info.versionCode;
        } catch (Exception e) {
            //e.printStackTrace();
        }
        return appVersion;
    }

    /* Method for showing the Alert popup */
    public static void showAppUpdatePopUp(final Context context, String string) {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);

        alertDialog.setTitle(context.getString(R.string.app_name));
        alertDialog.setMessage(string);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(context.getString(R.string.update), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                ((Activity) context).finish();

                String appPackageName = context.getPackageName();
                try {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });

        alertDialog.show();
    }

    /* Method for showing the Alert popup */
    public static void showAlertPopUpAndFinish(final Context context, final String string)
    {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);

        alertDialog.setTitle(context.getString(R.string.app_name));
        alertDialog.setMessage(string);

        alertDialog.setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
                //((Activity) context).finish();
            }
        });

        alertDialog.show();
    }

}
