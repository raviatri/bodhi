package com.rs.bodhi.utility;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {
    private static String ISFIRSTTIME = "IsFirstTime";
    private static String ACCESS_TOKEN = "AccessToken";
    private static String ISMOBILENUMBERVERIFIED = "IsMobileNumberVerified";
    private static String ISPROFILEUPDATED = "IsProfileUpdated";
    private static String MOBILENUMBER = "MobileNumber";
    private static String COUNTRYCODE = "CountryCode";

    private static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences("bodhi", 0);
    }

    public static boolean getIsFirstTime(Context context) {
        return getPrefs(context).getBoolean(ISFIRSTTIME, false);
    }

    public static void setIsFirstTime(Context context, boolean value) {
        getPrefs(context).edit().putBoolean(ISFIRSTTIME, value).apply();
    }

    // Get set method for AccessToken.
    public static String getAccessToken(Context context){
        return getPrefs(context).getString(ACCESS_TOKEN, Constants.BLANK_STRING);
    }
    public static void saveAccessToken(Context context, String value){
        getPrefs(context).edit().putString(ACCESS_TOKEN, value).apply();
    }

    // Get set value for Mobile Number verified.
    public static void setIsMobileNumberVerified(Context context, boolean value){
        getPrefs(context).edit().putBoolean(ISMOBILENUMBERVERIFIED, value).apply();
    }

    public static boolean getIsNumberVerified(Context context){
        return getPrefs(context).getBoolean(ISMOBILENUMBERVERIFIED, false);
    }

    // Getter setter method fo profile update.
    public static void setIsProfileUpdated(Context context, boolean value){
        getPrefs(context).edit().putBoolean(ISPROFILEUPDATED, value).apply();
    }

    public static boolean isProfileUpdated(Context context){
        return getPrefs(context).getBoolean(ISPROFILEUPDATED, false);
    }

    // Get set method for MobileNumber.
    public static String geMobileNumber(Context context){
        return getPrefs(context).getString(MOBILENUMBER, Constants.BLANK_STRING);
    }
    public static void saveMobileNumber(Context context, String value){
        getPrefs(context).edit().putString(MOBILENUMBER, value).apply();
    }

    // Get set method for Country Code.
    public static String getCountryCode(Context context){
        return getPrefs(context).getString(COUNTRYCODE, Constants.BLANK_STRING);
    }
    public static void saveCountryCode(Context context, String value){
        getPrefs(context).edit().putString(COUNTRYCODE, value).apply();
    }
}
