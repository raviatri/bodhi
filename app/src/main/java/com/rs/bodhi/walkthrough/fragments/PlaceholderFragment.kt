package com.rs.bodhi.walkthrough.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.rs.bodhi.R
import com.rs.bodhi.entermobilenumber.EnterMobileNumber
import com.rs.bodhi.tabbar.HomeActivity
import com.rs.bodhi.utility.Preferences

class PlaceholderFragment : Fragment() {

    private lateinit var pageViewModel: PageViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel::class.java).apply {
            setIndex(arguments?.getInt(ARG_SECTION_NUMBER) ?: 1)
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_main, container, false)
        val txtHeader: TextView = root.findViewById(R.id.txt_headed)
        val txtfooter: TextView = root.findViewById(R.id.txt_footer)
        val img: ImageView = root.findViewById(R.id.img);
        val btnSkip: Button = root.findViewById(R.id.btn_skip)

        val pageIndex = arguments?.getInt(ARG_SECTION_NUMBER) ?: 10;
        Log.d("pageindex", (pageIndex).toString());

        var headerTitile = "";
        var subHeaderTitile = "Some tag line will come over here";
        if (pageIndex == 1) {
            headerTitile = "Consult";
            img.setImageResource(R.drawable.asset_1_2)
        } else if (pageIndex == 2) {
            headerTitile = "Horoscope";
            img.setImageResource(R.drawable.asset_1_1)
        } else {
            headerTitile = "Wellness";
            img.setImageResource(R.drawable.asset_20_1)
        }
        txtHeader.setText(headerTitile);
        txtfooter.setText(subHeaderTitile);

        val registerBtn: Button = root.findViewById(R.id.btn_register);
        registerBtn.setOnClickListener(View.OnClickListener {
            // Handle action of register button.
            val intent = Intent(activity, EnterMobileNumber::class.java)
            startActivity(intent)
        })

        btnSkip.setOnClickListener(View.OnClickListener {
            // Handle action of register button.
            //  val intent = Intent(activity, EnterMobileNumber::class.java)
            val intent = Intent(activity, HomeActivity::class.java)
            Preferences.setIsFirstTime(activity,true)
            startActivity(intent)
            activity?.finish()
        })

        return root
    }

    companion object {

        private const val ARG_SECTION_NUMBER = "section_number"

        @JvmStatic
        fun newInstance(sectionNumber: Int): PlaceholderFragment {
            return PlaceholderFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }
}