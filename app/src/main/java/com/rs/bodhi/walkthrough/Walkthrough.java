package com.rs.bodhi.walkthrough;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;
import com.rs.bodhi.R;
import com.rs.bodhi.walkthrough.adapters.SectionsPagerAdapter;

public class Walkthrough extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walkthrough);

        SectionsPagerAdapter sectionsPagerAdapter = new
                SectionsPagerAdapter(
                        this,
                        getSupportFragmentManager()
                );

        //getSupportActionBar().hide();

        ViewPager viewPager = findViewById(R.id.viewPager);
                TabLayout tabLayout = findViewById(R.id.tabLayout);
                viewPager.setAdapter(sectionsPagerAdapter);
        tabLayout.setupWithViewPager(viewPager,true);
    }

}