package com.rs.bodhi.tabbar.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.rs.bodhi.R;
import com.rs.bodhi.entermobilenumber.EnterMobileNumber;
import com.rs.bodhi.knowmore.KnowMore;
import com.rs.bodhi.tabbar.HomeActivity;
import com.rs.bodhi.utility.Preferences;

public class ProfileFragment extends Fragment {
    public ProfileFragment() {
    }

    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString("arg1", param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String mParam1 = getArguments().getString("param1");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        Button loginRegister = (Button)view.findViewById(R.id.btn_login_register);
        loginRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Preferences.getIsNumberVerified(view.getContext())){
                    Intent intent = new Intent(getActivity(), KnowMore.class);
                    startActivity(intent);
                }else {
                    Intent intent = new Intent(getActivity(), EnterMobileNumber.class);
                    startActivity(intent);
                }
            }
        });

        return view;
    }

}