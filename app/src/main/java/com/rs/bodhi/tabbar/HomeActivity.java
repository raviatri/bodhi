package com.rs.bodhi.tabbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.rs.bodhi.R;
import com.rs.bodhi.tabbar.fragments.ConsultFragment;
import com.rs.bodhi.tabbar.fragments.HomeFragment;
import com.rs.bodhi.tabbar.fragments.HoroScopeFragment;
import com.rs.bodhi.tabbar.fragments.ProfileFragment;
import com.rs.bodhi.tabbar.fragments.WellnessFragment;

public class HomeActivity extends AppCompatActivity {
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        HomeFragment homeFragment = new HomeFragment();
        HoroScopeFragment horoScopeFragment = new HoroScopeFragment();
        ConsultFragment consultFragment = new ConsultFragment();
        ProfileFragment profileFragment = new ProfileFragment();
        WellnessFragment wellnessFragment = new WellnessFragment();
        setCurrentFragment(homeFragment);

        bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.home:
                        setCurrentFragment(homeFragment);
                        break;
                    case R.id.consult:
                        setCurrentFragment(consultFragment);
                        break;
                    case R.id.horoscope:
                        setCurrentFragment(horoScopeFragment);
                        break;
                    case R.id.wellness:
                        setCurrentFragment(wellnessFragment);
                        break;
                    case R.id.profile:
                        setCurrentFragment(profileFragment);
                        break;
                }
                return true;
            }
        });
    }

    private void setCurrentFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.flFragment, fragment);
        fragmentTransaction.commit();
    }
}