package com.rs.bodhi.splash;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.rs.bodhi.MainActivity;
import com.rs.bodhi.R;
import com.rs.bodhi.entermobilenumber.EnterMobileNumber;
import com.rs.bodhi.knowmore.KnowMore;
import com.rs.bodhi.tabbar.HomeActivity;
import com.rs.bodhi.utility.Constants;
import com.rs.bodhi.utility.Preferences;
import com.rs.bodhi.utility.Util;
import com.rs.bodhi.verifyotp.VerifyOtp;
import com.rs.bodhi.walkthrough.Walkthrough;
import com.rs.bodhi.vollyclient.VolleyService;
import com.rs.bodhi.vollyclient.IResult;

import org.json.JSONObject;

import androidx.appcompat.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {
    IResult mResultCallback = null;
    VolleyService mVolleyService;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splace);
        callApi();
//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//                if (Preferences.getIsFirstTime(SplashActivity.this)) {
//                    Intent i = new Intent(SplashActivity.this, HomeActivity.class);
//                    startActivity(i);
//                    finish();
//                } else {
//                    Intent i = new Intent(SplashActivity.this, Walkthrough.class);
//                    startActivity(i);
//                    finish();
//                }
//
//            }
//        },1000);
    }

    // Check build version and for force update app.
    private void callApi(){
        if (Util.isNetworkConnected(this)) {
            try {
                JSONObject object = new JSONObject();
                object.put("buildNumber", Util.getAppVersionCode(this));
                object.put("versionNumber", Util.getAppVersion(this));
                object.put("appName", Constants.APP_NAME);

                initVolleyCallback();
                mVolleyService = new VolleyService(mResultCallback, SplashActivity.this);
                dialog = new ProgressDialog(SplashActivity.this);
                dialog.setCancelable(false);
                dialog.setMessage("Please wait...");
                dialog.show();
                Util.logData("API:- " + Constants.FORCE_UPDATE);
                Util.logData("Request:- " + object.toString());
                mVolleyService.postDataVolley("1", Constants.FORCE_UPDATE, object.toString());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            Toast.makeText(this, "Please check your internet connectivity.", Toast.LENGTH_SHORT).show();
        }
    }

    void initVolleyCallback() {
        mResultCallback = new IResult() {

            @Override
            public void notifySuccess(String requestType, String response) {

                Util.logData("Response:- "+response);
                dialog.dismiss();
                parseResponse(response);
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                dialog.dismiss();
                Toast.makeText(SplashActivity.this, "Error occured", Toast.LENGTH_SHORT).show();
            }
        };
    }

    private void parseResponse(String response){
        try{
            JSONObject object = new JSONObject(response);

            if (object.getInt("statusCode") == 200){
                //Toast.makeText(this, object.getString("message"), Toast.LENGTH_SHORT).show();
                JSONObject payLoad = object.getJSONObject(Constants.PAY_LOAD);
                boolean isAlreadyUpdated = payLoad.optBoolean(Constants.ALREADY_UPDATED_WS, true);
                if (isAlreadyUpdated){
                    checkAndMoveToRespectedScene();
                }else{
                    boolean isForceUpdate = payLoad.optBoolean(Constants.FORCE_UPDATE, false);
                    if (isForceUpdate){
                        // Prompt force update dialogue.
                        Util.showAppUpdatePopUp(this, payLoad.optString(Constants.FORCE_UPDATE_MESSAGE));
                    }else{
                        checkAndMoveToRespectedScene();
                        Toast.makeText(this, R.string.app_new_version_available, Toast.LENGTH_SHORT).show();
                    }
                }
            }else{
                Toast.makeText(this, object.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    private void checkAndMoveToRespectedScene() {
        if (Preferences.getIsNumberVerified(SplashActivity.this)) {
            Intent i = new Intent(SplashActivity.this, HomeActivity.class);
            startActivity(i);
            finish();
        } else {
            Intent i = new Intent(SplashActivity.this, Walkthrough.class);
            startActivity(i);
            finish();
        }
    }


}
